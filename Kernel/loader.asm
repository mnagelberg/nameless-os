global loader
extern main
extern initializeKernelBinary
extern keyboard_handler
extern sys_read
extern sys_write
extern sys_malloc
extern sys_free
extern sys_cls
extern sys_exit
extern sys_sleep

;definicion de syscalls ids
READ equ 1
WRITE equ 2
MALLOC equ 3
FREE equ 4
CLS equ 5
EXIT equ 6
SLEEP equ 7

section .text

loader:
        call initializeKernelBinary	; Set up the kernel binary, and get thet stack address
	mov rsp, rax				; Set up the stack with the returned address
        call set_idt_handlers
        call start_pic
	call main
hang:
	hlt							; halt machine should kernel return
	jmp hang

;Inicializo el PIC
start_pic:    
    sti                 ;Habilito las interrupciones
    ret

;Configuro la IDT
set_idt_handlers:
    mov rax, pit_hand
    mov rdi, 0x20
    call set_gate        ;seteo la interrupcion del PIT en el puerto 20h de la IDT

    mov rax, keyboard_hand
    mov rdi, 0x21
    call set_gate        ;seteo la interrupcion de teclado en 21h
    
    mov rax, soft_int
    mov rdi, 0x80
    call set_gate        ;seteo las interrupciones de software en 80h
    
    lidt [IDTRx64]       ;configuro el IDTR
    ret

;Seteo el puerto (gate) de cierta interrupcion
set_gate:
    push rax
    push rdi
    
    shl rdi, 4        ;RDI apunta a dir de memoria de la gate del IDT
    stosw               ;guardo AX en [RDI] (15....0)
    
    add rdi, 4          ;me salto el segment selector y flags del Gate
    shr rax, 16
    stosw               ;guardo AX (31...16)
    shr rax, 16
    stosd               ;guardo EAX (63-32)
    
    
    pop rdi
    pop rax
    ret

;Handler del PIT (el PIT manda por default una interrupcion con ~18Hz de frecuencia)
;entonces puedo usarlo como contador de segs.
ALIGN 8
pit_hand:
    push rax
    
    ;call "algo que necesite depender del tiempo"
    
    mov al, 0x20
    out 0x20, al        ;mando EOI al PIC (master)
    
    pop rax
    iretq

;Handler del teclado.
;Levanto el scan code y se la paso a mi handler en C.
ALIGN 8
keyboard_hand:
    push rax
    push rdi
    
    xor rax, rax
    in al, 0x60        ;levanto el scan code del teclado
    
    mov rdi, rax
    call keyboard_handler
    
    mov al, 0x20
    out 0x20, al       ;mando EOI al PIC
    
    pop rdi
    pop rax
    iretq

;Handler de la interrupciones de soft.
;Recibe por RAX el codigo de la int y despues por RDI, RSI, etc los params.
ALIGN 8
soft_int:
    cmp rax, READ
    jz .sys_read_l
    
    cmp rax, WRITE
    jz .sys_write_l

    cmp rax, MALLOC
    jz .sys_malloc_l

    cmp rax, FREE
    jz .sys_free_l

    cmp rax, CLS
    jz .sys_cls_l

    cmp rax, EXIT
    jz .sys_exit_l
 
    cmp rax, SLEEP 
    jz .sys_sleep_l
    
    jmp .end
    
.sys_read_l:
    call sys_read
    jmp .end
    
.sys_write_l:
    call sys_write
    jmp .end

.sys_malloc_l:
    call sys_malloc
    jmp .end

.sys_free_l:
    call sys_free
    jmp .end

.sys_cls_l:
    call sys_cls
    jmp .end

.sys_exit_l:
    call sys_exit
    jmp .end

.sys_sleep_l:
    call sys_sleep
    jmp .end

.end:
    push rax
    
    mov al, 0x20
    out 0x20, al        ;EOI al PIC
    
    pop rax
    iretq

section .rodata
IDTRx64: dw 256*16-1       ;Limite de la IDT (4KB - 1)
         dq 0x0            ;Direccion de la IDT (Pure64 la carga en 0x0000000000000000)
