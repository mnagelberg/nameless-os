#ifndef LIB_H
#define LIB_H

#include <stdint.h>
#include <typeDefs.h>

void * memset(void * destination, int32_t character, uint64_t length);
void * memcpy(void * destination, const void * source, uint64_t length);
void* malloc(size_t size);
void free(void* m);

char *cpuVendor(char *result);

#endif
