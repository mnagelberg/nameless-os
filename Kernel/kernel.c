#include <stdint.h>
#include <video.h>
#include <lib.h>
#include <moduleLoader.h>
#include <naiveConsole.h>

extern uint8_t text;
extern uint8_t rodata;
extern uint8_t data;
extern uint8_t bss;
extern uint8_t endOfKernelBinary;
extern uint8_t endOfKernel;

static const uint64_t PageSize = 0x1000;

static void * const sampleCodeModuleAddress = (void*)0x400000;
static void * const sampleDataModuleAddress = (void*)0x500000;

typedef int (*EntryPoint)();


void clearBSS(void * bssAddress, uint64_t bssSize)
{
	memset(bssAddress, 0, bssSize);
}

void * getStackBase()
{
	return (void*)(
		(uint64_t)&endOfKernel
		+ PageSize * 8				//The size of the stack itself, 32KiB
		- sizeof(uint64_t)			//Begin at the top of the stack
	);
}

void * initializeKernelBinary()
{
	char buffer[10];

	//ncPrint("[x64BareBones]");
	//ncNewline();

	//ncPrint("CPU Vendor:");
	//ncPrint(cpuVendor(buffer));
	//ncNewline();

	//ncPrint("[Loading modules]");
	//ncNewline();
	void * moduleAddresses[] = {
		sampleCodeModuleAddress,
		sampleDataModuleAddress
	};

	loadModules(&endOfKernelBinary, moduleAddresses);
	//ncPrint("[Done]");
	//ncNewline();
	//ncNewline();

	//ncPrint("[Initializing kernel's binary]");
	//ncNewline();

	clearBSS(&bss, &endOfKernel - &bss);

	///ncPrint("  text: 0x");
	//ncPrintHex((uint64_t)&text);
	//ncNewline();
	//ncPrint("  rodata: 0x");
	//ncPrintHex((uint64_t)&rodata);
	///ncNewline();
	//ncPrint("  data: 0x");
	//ncPrintHex((uint64_t)&data);
	//ncNewline();
	//ncPrint("  bss: 0x");
	//ncPrintHex((uint64_t)&bss);
	//ncNewline();

	//ncPrint("[Done]");
	//ncNewline();
	//ncNewline();
	return getStackBase();
}

int main()
{	
	clear_screen();	 
	print_title();    
	print_line();
	print_line();
                                                
	//print_string("[Kernel Main]\n");
	//print_string("  Sample code module at 0x");
	//print_num_hex((uint64_t)sampleCodeModuleAddress);
	//print_line();
	print_string("SHELL iniciada correctamente.");
	print_line();
	print_num_hex(((EntryPoint)sampleCodeModuleAddress)());
	

	//print_string("  Sample data module at 0x");
	//print_num_hex((uint64_t)sampleDataModuleAddress);
	//print_line();
	//print_string("  Sample data module contents: ");
	//print_string((char*)sampleDataModuleAddress);
	//print_line();

	//print_string("[Finished]");
	//print_line();
	//print_line();

	//print_title();

	//print_line();
	//print_enter();               
	
	while (1){

	}
	
	return 0;
}

void print_title(){
	print_string("\t\t _   _                      _                 _____ _____\n");
	print_string("\t\t| \\ | |                    | |               |  _  /  ___|\n");
	print_string("\t\t|  \\| | __ _ _ __ ___   ___| | ___  ___ ___  | | | \\ `--. \n");
	print_string("\t\t| . ` |/ _` | '_ ` _ \\ / _ \\ |/ _ \\/ __/ __| | | | |`--. \\\n");
	print_string("\t\t| |\\  | (_| | | | | | |  __/ |  __/\\__ \\__ \\ \\ \\_/ /\\__/ /\n");
	print_string("\t\t\\_| \\_/\\__,_|_| |_| |_|\\___|_|\\___||___/___/  \\___/\\____/\n");
}
