//Usamos _syscall en asm para llamar a int 0x80. 

#include <syscalls.h>
#include <typeDefs.h>
#include <stdint.h>

extern uint64_t _syscall(uint64_t callid, ...);

int sys_read(FILE_DESC fd, char * string, size_t length){

	return _syscall((uint64_t)READ, (uint64_t) fd, (uint64_t) string, (uint64_t) length);
}

void sys_write(FILE_DESC fd, const char * string, size_t length){
	
	_syscall((uint64_t)WRITE, (uint64_t) fd, (uint64_t) string, (uint64_t) length);
}

void* sys_malloc(size_t size){

	return (void*) _syscall((uint64_t)MALLOC, (uint64_t) size);
}

void sys_free(void* p){

	_syscall((uint64_t)FREE, (uint64_t) p);
}

void sys_cls(){
	
	_syscall((uint64_t)CLS);
}

void sys_exit(int exit_code) {

	_syscall((uint64_t)EXIT, (uint64_t) exit_code);
}

void sys_sleep(size_t ms){

	_syscall((uint64_t)SLEEP, (uint64_t) ms);
}


