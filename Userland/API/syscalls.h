#ifndef SYSCALLS_HEADER
#define SYSCALLS_HEADER

#include <typeDefs.h>

typedef enum {
READ = 1,
WRITE = 2,
MALLOC = 3,
FREE = 4,
CLS = 5,
EXIT = 6,
SLEEP = 7
} syscall_id;

int sys_read(FILE_DESC fd, char * string, size_t length);
void sys_write(FILE_DESC fd, const char * string, size_t length);
void* sys_malloc(size_t size); //no deberia ser una sys_call, pero para no implementar mmap
void sys_free(void* p);
void sys_cls();
void sys_exit(int exit_code);
void sys_sleep(size_t ms);

#endif
