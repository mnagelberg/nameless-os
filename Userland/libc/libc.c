#include <typeDefs.h>
#include <syscalls.h>
#include <stdarg.h>
#include <stdint.h>
#include <libc.h>

char buffer2[64] = {0};

static uint32_t uintToBase(uint64_t value, char * buffer, uint32_t base);

int strlen(char * str){
	int i = 0;
	
	while (str[i] != 0) i++;
	
	return i;
}
 //ver de cambiar
int atoi(char* c) {
	
	int d, len, i;

	d = 0;
	i = 0;
	len = strlen(c);
	
	while  (i < len) {
		d = d * 10 + c[i] - '0';
		i++;
	}

	return d;
}

//ver de cambiar
int strcmp(char * cad1, char * cad2){

	int i;
	int len1 = strlen(cad1);
	int len2 = strlen(cad2);

	if(len1 != len2){
		return len1 - len2;
	}

	for(i = 0; i < len1; i++){
		if(cad1[i] != cad2[i]){
			if (cad1[i] > cad2[i])
				return 1;
			else
				return -1;
		}
	}
	return 0;
}


void* malloc(int length) {
	return sys_malloc(length);
}

void free(void* p){

	sys_free(p);

}

void printf(char* fmt, ...) {

	va_list arguments;

	if (fmt == NULL) return;
	
	int len = strlen(fmt);
	int num;
	char c;
	char* string;
	va_start(arguments, len);
	
	//no generamos cadena formateada para hacerlo mas simple.
	
	for (int i = 0; i < len; i++){
		
		if (fmt[i] != '%'){
			sys_write(STDOUT, (char*) (&(fmt[i])), 1);
			continue;
		}
		
		switch (fmt[i+1]) {
				
			case 'd':
				num = (char) va_arg(arguments, int);
				sys_write(STDOUT, &num, 1);
				break;
				
			case 'c':
				c = (char) va_arg(arguments, char);
				sys_write(STDOUT, &c, 1);
				break;
		
			case 's':
				string = (char*) va_arg(arguments, char*);
				sys_write(STDOUT, string, strlen(string));
				break;
		}
		
		i++; //Me salteo el caracter que me dice el tipo del formateo
					
	}
	

	va_end(arguments);

}

void putchar(const char c) {
	sys_write(STDOUT, &c, 1);
}


char getchar() {

	static char buffer[2] = {0};

	char read = sys_read(STDOUT, buffer, 1);

	if (read == 0) {
		return 0;
	}

	return buffer[0];
}

int scanf(char* c, int length) {

	char read = sys_read(STDOUT, c, length);

	return read;
}


/*void clear_screen() {
	sys_clear_screen();
}*/

int exit(int exit_code) {
	sys_exit(exit_code);
}

/**
 * Le hago cambio de base a un numero y lo pongo en mi buffer
 * Ref: naiveConsole.c (Catedra Arqui)
 */
static uint32_t uintToBase(uint64_t value, char * buffer, uint32_t base){
	char *p = buffer;
	char *p1, *p2;
	uint32_t digits = 0;

	//Calculate characters for each digit
	do
	{
		uint32_t remainder = value % base;
		*p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
		digits++;
	}
	while (value /= base);

	// Terminate string in buffer.
	*p = 0;

	//Reverse string in buffer.
	p1 = buffer;
	p2 = p - 1;
	while (p1 < p2)
	{
		char tmp = *p1;
		*p1 = *p2;
		*p2 = tmp;
		p1++;
		p2--;
	}

	return digits;
}
