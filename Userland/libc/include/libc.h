#ifndef LIBC_HEADER
#define LIBC_HEADER

#include <stdint.h>

int strlen(char * str);
int atoi(char* c);
int strcmp(char * str1, char * str2);
void* malloc(int length);
void free(void* p);
void printf(char* fmt, ...);
void putchar(const char c);
char getchar();
int scanf(char* c, int length);
void clear_screen();
int exit(int exit_code);

#endif

